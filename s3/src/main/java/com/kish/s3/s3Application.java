package com.kish.s3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class s3Application {

	public static void main(String[] args) {
		SpringApplication.run(s3Application.class, args);
	}

}