package com.kish.s3;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.fasterxml.jackson.databind.ObjectMapper;


@Controller	// This means that this class is a Controller
@RequestMapping(path="/s3") // This means URL's start with /demo (after Application path)
public class s3Controller {

	@Value("${aws.accessid}")
	String aws_accessid;

	@Value("${aws.key}")
	String aws_key;

	@Value("${aws.bucketname}")
	String aws_bucket_name;

	@Value("${user_host_ip}")
	String user_host_ip;

	@Autowired
	ServletContext context;

	@PostMapping(path="/upload") // Map ONLY POST Requests
	public @ResponseBody String uploads3 (@RequestHeader String email
			,@RequestHeader String password, @RequestParam("file") MultipartFile file) throws IOException, Exception {


		HttpClient client = HttpClient.newHttpClient();
		var values = new HashMap<String, String>() {{
			put("name", "<>");
		}};

		var objectMapper = new ObjectMapper();
		String requestBody = objectMapper
				.writeValueAsString(values);
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create("http://"+user_host_ip+"/users/login?email="+email+"&password="+password))
				//.setHeader("api-key", "value")
				.POST(HttpRequest.BodyPublishers.ofString(requestBody))
				.build();

		HttpResponse<String> response = client.send(request,
				HttpResponse.BodyHandlers.ofString());
		System.out.println(response.body());
		if(response.statusCode()==200) {


			String absolutePath = context.getRealPath("resources/uploads");
			File uploadedFile = new File(absolutePath, "new.xls");

			System.out.println("userhst: "+user_host_ip);
			ObjectMetadata data = new ObjectMetadata();
			data.setContentType(file.getContentType());
			data.setContentLength(file.getSize());
			BasicAWSCredentials creds = new BasicAWSCredentials(aws_accessid, aws_key);
			AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).withCredentials(new AWSStaticCredentialsProvider(creds)).build();
			PutObjectResult objectResult = s3client.putObject(aws_bucket_name, file.getOriginalFilename(), file.getInputStream(), data);
			System.out.println(objectResult.getContentMd5()); //you can verify MD5
			
			return "S3 Uploaded Successfully";

		} else {
			return "Invalid Credentials in Headers";
		}




	}

}
